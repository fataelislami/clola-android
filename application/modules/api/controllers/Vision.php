<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vision extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    header('Content-Type: application/json');
    $this->load->library('instagramfunc');


  }


  function image(){
    if(isset($_GET['url'])){
      $url=$_GET['url'];
    }else{
      echo "Error Params";
      die;
    }
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://vision.googleapis.com/v1/images:annotate?key=AIzaSyBMNYhWCm65MIgTPHGkfykIeGOSPRaNIyU",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "{\n \"requests\":[\n    {\n      \"image\":{\n      \"source\":{\n      \"imageUri\":\n      \"$url\"\n         }\n     },\n       \"features\": [\n       {\n         \"type\": \"WEB_DETECTION\",\n         \"maxResults\": 2\n     }\n    ]\n   }\n  ]\n}",
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Postman-Token: 568bac5d-f326-4df6-b7d9-5afa611ae348",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      $obj=json_decode($response);
      $newArr = [];
      $strTempMax=$obj->responses[0]->webDetection->webEntities[0]->description;
      $strTemp=$obj->responses[0]->webDetection->webEntities[0]->description;
      foreach ($obj->responses[0]->webDetection->webEntities as $obj) {
        if(strlen($obj->description)<$strTempMax){
          $strTemp=$obj->description;
        }
      }
      $this->instagramfunc->hashtag($strTemp);
      // $newJson = array('result' => $strTemp );
      // echo json_encode($newJson);
    }
  }

  function imagebase64(){
    if(isset($_POST['base64'])){
      $base64=$_POST['base64'];
    }else{
      echo "Error Params";
      die;
    }
    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => "https://vision.googleapis.com/v1/images:annotate?key=AIzaSyBMNYhWCm65MIgTPHGkfykIeGOSPRaNIyU",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "{\n  \"requests\":[\n    {\n      \"image\":{\n        \"content\": \"$base64\"\n      },\n      \"features\": [\n        {\n          \"type\":\"WEB_DETECTION\",\n          \"maxResults\":2\n        }\n      ]\n    }\n  ]\n}",
    CURLOPT_HTTPHEADER => array(
      "Content-Type: application/json",
      "Postman-Token: 3ac045ee-5228-49ae-8d59-47b294ae1568",
      "cache-control: no-cache"
    ),
  ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      // echo $response;
      $obj=json_decode($response);
      $newArr = [];
      $strTempMax=$obj->responses[0]->webDetection->webEntities[0]->description;
      $strTemp=$obj->responses[0]->webDetection->webEntities[0]->description;
      foreach ($obj->responses[0]->webDetection->webEntities as $obj) {
        if(strlen($obj->description)<$strTempMax){
          $strTemp=$obj->description;
        }
      }
      $this->instagramfunc->hashtag($strTemp);
      // $newJson = array('result' => $strTemp );
      // echo json_encode($newJson);
    }
  }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    header('Content-Type: application/json');
    $this->load->model(array('Dbs'));
  }

  function index()
  {

  }

  function create(){
    if(isset($_POST['caption']) and isset($_POST['id_user'])){//params yang akan dicek
      //default fungsi dari : getdata($table,$where=null,$limit=9,$offset=0){
      $caption=$_POST['caption'];
      $id_user=$_POST['id_user'];
      $foto=$this->upload_foto('photo');
      $filename=$foto['file_name'];

      $dataInsert=array(
        'caption'=>$caption,
        'id_user'=>$id_user
      );
      if($filename==''){
        $dataInsert['photo']=null;
      }else{
        $dataInsert['photo']=$foto['file_name'];
      }
      $table='posts';
      $sql=$this->Dbs->insert($dataInsert,$table);
      if($sql){
        // $get=$loadDb->result(); //Uncomment ini untuk contoh
        $data=array(
          'status'=>'success',
          'message'=>'Post telah disimpan',
          'photo'=>$foto['file_name'],
          // 'results'=>$get //Uncomment ini untuk contoh
        );
      }else{
        $data=array(
          'status'=>'failed',
          'message'=>'Post gagal disimpan',
          'photo'=>null,
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function read(){
      //StartPagination
      if(isset($_GET['page'])){//cek parameter page
        $page=$_GET['page'];
      }else{
        $page=1;//default jika parameter page tidak diload
      }
      $limitDb=9;
      $offsetDb=0;
      if($page!=1 and $page!=0){
        $offsetDb=$limitDb*($page-1);
      }
      //End Pagination
      //default fungsi dari : getdata($table,$where=null,$limit=9,$offset=0){
      $table='posts';
      if(isset($_GET['id'])){
        $where=array('id'=>$_GET['id']);
      }else if(isset($_GET['id_user'])){
        $where=array('id_user'=>$_GET['id_user']);
      }
      $loadDb=$this->Dbs->getdata($table,$where,$limitDb,$offsetDb);//database yang akan di load
      $check=$loadDb->num_rows();
      if($check>0){
        $get=$loadDb->result(); //Uncomment ini untuk contoh
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$check,
          'results'=>$get
          // 'results'=>$get //Uncomment ini untuk contoh
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'not found'
        );
      }
    $json=json_encode($data);
    echo $json;
  }

  function update(){
    if(isset($_POST['id']) and isset($_POST['caption'])){//params yang akan dicek
      //default fungsi dari : getdata($table,$where=null,$limit=9,$offset=0){
      $id=$_POST['id'];
      $caption=$_POST['caption'];
      $dataUpdate=array(
        'caption'=>$caption,
      );
      $table='posts';
      $sql=$this->Dbs->update($dataUpdate,$table,array('id'=>$id));
      if($sql){
        // $get=$loadDb->result(); //Uncomment ini untuk contoh
        $data=array(
          'status'=>'success',
          'message'=>'Caption telah diupdate',
          // 'results'=>$get //Uncomment ini untuk contoh
        );
      }else{
        $data=array(
          'status'=>'failed',
          'message'=>'Caption tidak diupdate'
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function delete(){
    if(isset($_POST['id'])){//params yang akan dicek
      //default fungsi dari : getdata($table,$where=null,$limit=9,$offset=0){
      $id=$_POST['id'];
      $table='posts';
      $this->Dbs->delete($table,array('id'=>$id));
      $data=array(
          'status'=>'success',
          'message'=>'Berhasil dihapus',
        );
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

  public function upload_foto($formname){
  $config['upload_path']          = './xfile/posts';
  $config['allowed_types']        = 'gif|jpg|png|jpeg';
  $config['encrypt_name'] = TRUE;
  //$config['max_size']             = 100;
  //$config['max_width']            = 1024;
  //$config['max_height']           = 768;
  $this->load->library('upload', $config);
  $this->upload->do_upload($formname);
  return $this->upload->data();

  //Cara pemakaian
  //hidupkan object terlebih dahulu
  //misal
  //$foto=$this->upload_foto();
  }

}

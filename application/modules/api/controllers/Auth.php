<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs'));
    header('Content-Type: application/json');
  }

  function index()
  {

  }

  function login(){
    if(isset($_POST['email']) and isset($_POST['password'])){//params yang akan dicek
      //default fungsi dari : getdata($table,$where=null,$limit=9,$offset=0){
      $email=$_POST['email'];
      $password=$_POST['password'];
      $table='users';
      $loadDb=$this->Dbs->getdata($table,array('email'=>$email,'password'=>sha1($password)));//database yang akan di load
      $check=$loadDb->num_rows();
      if($check>0){
        $get=$loadDb->row(); //Uncomment ini untuk contoh
        $data=array(
          'status'=>'success',
          'message'=>'Login Berhasil',
          'total_result'=>$check,
          'results'=>$get
          // 'results'=>$get //Uncomment ini untuk contoh
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'Email dan password salah'
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function register(){
    if(isset($_POST['name']) and isset($_POST['email']) and isset($_POST['password'])){//params yang akan dicek
      //default fungsi dari : getdata($table,$where=null,$limit=9,$offset=0){
      $name=$_POST['name'];
      $email=$_POST['email'];
      $password=$_POST['password'];
      $table='users';
      $loadDb=$this->Dbs->getdata($table,array('email'=>$email));//database yang akan di load
      $check=$loadDb->num_rows();
      if($check>0){
        $get=$loadDb->row(); //Uncomment ini untuk contoh
        $data=array(
          'status'=>'success',
          'message'=>'Email sudah digunakan',
          'total_result'=>$check,
          // 'results'=>$get //Uncomment ini untuk contoh
        );
      }else{
        $dataInsert=array(
          'name'=>$name,
          'email'=>$email,
          'password'=>sha1($password)
        );
        $sql=$this->Dbs->insert($dataInsert,'users');
        if($sql){
          $data=array(
            'status'=>'success',
            'message'=>'Pendaftaran berhasil, silakan login',
            'total_result'=>0
          );
        }
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }


}

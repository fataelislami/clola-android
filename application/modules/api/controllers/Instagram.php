<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instagram extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->library('instagramfunc');
    header('Content-Type: application/json');
  }

  function index()
  {
    echo "api";
  }

  function auth(){
    var_dump($this->instagramfunc->auth());
  }

  function myprofile(){
    $this->instagramfunc->getCurrentUser();
  }


  //Hashtag
  function hashtag(){
    if(isset($_GET['key'])){
      $key=$_GET['key'];
      $this->instagramfunc->hashtag($key);
    }else{
      echo "Invalid Params";
    }
  }
  function relatedHashtag(){
    $this->instagramfunc->relatedHashtag("kamera");
  }
  //Hashtag

  function location(){
    if(isset($_GET['lat']) and isset($_GET['lng'])){
      $this->instagramfunc->location($_GET['lat'],$_GET['lng']);
    }else{
      echo "Invalid Params";
    }
  }

  function postbylocation(){
    $idLocation=103847742987074;
    $this->instagramfunc->mediabyLocation($idLocation);
  }

}

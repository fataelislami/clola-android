<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function index()
  {

  }

  function image(){
    $foto=$this->upload_foto('photo');
    $filename=$foto['file_name'];
    if($filename==''){
      $data['response']='gagal';

    }else{
      $data['response']=$foto['file_name'];
    }
    $json=json_encode($data);
    echo $json;
  }

  public function upload_foto($formname){
  $config['upload_path']          = './xfile/posts';
  $config['allowed_types']        = 'gif|jpg|png|jpeg';
  $config['encrypt_name'] = TRUE;
  //$config['max_size']             = 100;
  //$config['max_width']            = 1024;
  //$config['max_height']           = 768;
  $this->load->library('upload', $config);
  $this->upload->do_upload($formname);
  return $this->upload->data();

  //Cara pemakaian
  //hidupkan object terlebih dahulu
  //misal
  //$foto=$this->upload_foto();
  }

}
